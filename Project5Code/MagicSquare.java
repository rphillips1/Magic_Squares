import java.util.ArrayList;

/**
 * Provides a method to determine if an array is a Magic Square.
 * 
 * @author Karl R. Wurst
 * @version Project 5
 */
public class MagicSquare
{
    /**
     * Determines if an array is a Magic Square.
     * 
     * @param square a two-dimensional array of ints
     * @return true if the array is a Magic Square
     */
    public static boolean isMagicSquare(int[][] square) {
        /* If the array is not square and 
         * does not contain all numbers from 1 to n*n,
         * where n is the length of the sides,
         * it's not a Magic Square
         */
        if (!isSquare(square) || !containsAllNumbers(square)) {
            return false;
        }

        /* Get the sum of the first diagonal, and remember it
         * If the other diagonal's sum doesn't match, it's not a Magic Square
         */
        int sum = diagonal1Sum(square);
        if (sum != diagonal2Sum(square)) {
            return false;
        }

        // If the sums of each of the rows does not match, it's not a Magic Square
        if (sum != rowSums(square)) {
            return false;
        }

        // If the sums of each of the columns does not match, it's not a Magic Square
        if (sum != colSums(square)) {
            return false;
        }

        // Passed all tests - it is a Magic Square
        return true;
    }

    /**
     * Determines if an array is a square.
     * 
     * @param square a two-dimensional array of ints
     * @return true if the array is a square (all rows and cols are same length, and not zero length)
     */
    public static boolean isSquare(int[][] array) {
        int rows = array.length;
        if (rows == 0) {
            return false;
        }

        boolean square = true;
        for (int col = 0; col < rows; col++) {
            if (array[col].length != rows) {
                square = false;
            }
        }
        return square;
    }

    /**
     * Determines if the array contain all numbers from 1 to n*n,
     * where n is the length of the sides
     * 
     * @param square a square, two-dimensional array of ints
     * @return true if the array contain all numbers from 1 to n*n
     */
    public static boolean containsAllNumbers(int[][] square) {
        ArrayList<Integer> numList = new ArrayList<Integer>();
        for (int row = 0; row < square.length; row++) {
            for (int col = 0; col < square[row].length; col++) {
                numList.add(square[row][col]);
            }
        }

        boolean all = true;
        for (int num = 1; num <= square.length * square.length; num++) {
            if (numList.indexOf(num) == -1) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the sum of the first diagonal
     * (upper-left to lower-right)
     * 
     * @param square a square, two-dimensional array of ints
     * @return sum of the numbers in the diagonal
     */
    public static int diagonal1Sum(int[][] square) {
        int size = square.length;
        int total = 0;

        for(int i = 0; i < size; i++) {
            total += square[i][i];
        }
        return total; 
    }

    /**
     * Get the sum of the second diagonal
     * (upper-right to lower-left)
     * 
     * @param square a square, two-dimensional array of ints
     * @return sum of the numbers in the diagonal
     */
    public static int diagonal2Sum(int[][] square) {
        int size = square.length;
        int total = 0;

        for(int i = 0; i < size; i++) {
            total += square[size - 1 - i][i];
        }
        return total; 
    }

    /**
     * Get the sum of each row, if they are all the same
     * 
     * @param square a square, two-dimensional array of ints
     * @return -1 if the sums don't match, sum of the first row otherwise
     */
    public static int rowSums(int[][] square) {
        int size = square.length;
        int sum = -1;
        int row = 0;

        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                row += square[i][j];
            }

            if(sum == -1) {
                sum = row;
            }
            else if(sum != row) {
                return -1;
            }
            row = 0; // reset the aggregator variable
        }
        return sum;
    }

    /**
     * Get the sum of each col, if they are all the same
     * 
     * @param square a square, two-dimensional array of ints
     * @return -1 if the sums don't match, sum of the first col otherwise
     */
    public static int colSums(int[][] square) {
        int size = square.length;
        int sum = -1;
        int col = 0;

        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                col += square[j][i];
            }

            if(sum == -1) {
                sum = col;
            }
            else if(sum != col) {
                return -1;
            }
            col = 0; // reset the aggregator variable
        }
        return sum;
    }

    /**
     * Get the sum of a given row
     * 
     * @param square a square, two-dimensional array of ints
     * @return sum of the numbers in the given row
     */
    public static int rowSum(int[][] square, int row) {
        int size = square.length;
        int sum = 0;
        
        for(int i = 0; i < size; i++) {
            sum += square[row][i];
        }
        return sum; 
    }

    /**
     * Get the sum of a given column
     * 
     * @param square a square, two-dimensional array of ints
     * @return sum of the numbers in the given column
     */
    public static int colSum(int[][] square, int col) {
        int size = square.length;
        int sum = 0;
        
        for(int i = 0; i < size; i++) {
            sum += square[i][col];
        }
        return sum; 
    }
}
